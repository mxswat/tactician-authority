local data = SkillTreeTweakData.init
function SkillTreeTweakData:init(tweak_data)
	data(self, tweak_data)
	local digest = function(value)
		return Application:digest_value(value, true)
	end

	-- I like to use table.insert because self.skill_pages_order is a table and 
	-- using the insert it won't overrie the skill tree of the other modders
	-- log(tostring(type(self.skill_pages_order)))
	-- I use contain_my_skilltree for check if there is a mine skill tree (example:added by another my mod)
	local function contain_my_skilltree (tab, val)
	    for index, value in ipairs(tab) do
	        if value == val then
	            return true
	        end
	    end
	    return false
	end

	-- Clone the skills -- Compatibility with YOS
	if not check_once then
		for key, value in pairs(self.skills) do -- pairs get also not numeric keys
			if string.sub(key, 1, 6) ~= 'YOS_' then
				self.skills["YOS_"..tostring(key)] = self.skills[key]
			end
		end
		log('Double_check')
		for key, value in pairs(self.skills) do -- pairs get also not numeric keys
			if string.sub(key, 1, 6) ~= 'YOS_' then
				self.skills["YOS_"..tostring(key)] = self.skills[key]
			end
		end
		check_once = true
	end

	-- Exec -> contain_my_skilltree
	if not contain_my_skilltree(self.skill_pages_order,"predator") then
		table.insert(self.skill_pages_order,"predator")
		self.skilltree.predator = {
			name_id = "predator_skilltree",
			desc_id = "predator_skilltree_desc"
		}
	end

-- This is for insert our skill tre in main tree
table.insert(self.trees,{
			name_id = "predator_tacticians_authority",-- id for localization
			background_texture = "guis/textures/pd2/skilltree/bg_mastermind",
			unlocked = true,
			skill = "predator", -- the name of your skilltree
			tiers = {
						{
							"YOS_hardware_expert"
						},
						{
							"YOS_fire_control",
							"YOS_drill_expert"
						},
						{
							"YOS_jack_of_all_trades",
							"YOS_ecm_booster"
						},
						{
							"YOS_ecm_2x"}

					}
			})	

end